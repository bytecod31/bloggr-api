<!DOCTYPE html>
<html lang='en'>

    <head>
        <meta charset="utf-8">
        <title>Books filter</title>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

    </head>

    <body>
        <form action="" id="filter">
            Comedy: <input type="checkbox" name="genre[]" value="comedy" id="">
            Drama: <input type="checkbox" name="genre[]" value="drama"><br>
            Fantasy: <input type="checkbox" name="genre[]" value="fantasy"><br>
            Horror: <input type="checkbox" name="genre[]" value="horror"><br>
            Philosophy: <input type="checkbox" name="genre[]" value="philosophy">

        </form> 

        <h3>
            Results
        </h3>

        <div id="books">

        </div>

        <script>
            $(function(){
                $("input[type=checkbox]").on('click', function(){
                    var books = '';
                    $("#books").html('loading...');
                    $.post(
                        'books/books',
                         $("#filter").serialize(),
                        function(data){
                            $.each(data, function(){
                                books += this.name + ' by ' + this.author + ' (' + this.genre + ')<br>';

                            });

                            $("#books").html(books);
                        });
                });
            });
        </script>
    </body>
</html>