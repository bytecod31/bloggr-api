@extends('layout.app')

@section('main')

    <div>
        @foreach ($books as $book)
            <div>Title: {{$book->title}}</div> 
            <div>Author: {{$book->author}}</div> 
            <div>Genre: {{$book->genre}}</div> 
        @endforeach
    </div>

@endsection