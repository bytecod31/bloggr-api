@extends('layout.app')

@section('main')
<form action="/books/store" method="POST">
    @csrf
    <div class="p-4 mb-4">
        {{--project title--}}
        <label class="font-extrabold" for="title">Title</label> <span class="text-red-600">*</span> <br>
        <input class="border-2 w-full rounded-md " type="text" placeholder="Title" name="name" >

    </div>

    <div class="p-4 mb-4">
        {{--project title--}}
        <label class="font-extrabold" for="title">Author</label> <span class="text-red-600">*</span> <br>
        <input class="border-2 w-full rounded-md " type="text" placeholder="Author" name="author" >
        
    </div>

    <div class="p-4 mb-4">
        {{--project title--}}
        <label class="font-extrabold" for="title">Genre</label> <span class="text-red-600">*</span> <br>
        <input class="border-2 w-full rounded-md " type="text" placeholder="Genre" name="genre" >
        
    </div>

    <div class="p-4 mb-4">
        {{--project title--}}
        <input class="border-2 w-full rounded-md " type="submit" value="Save">
        
    </div>
</form>

@endsection