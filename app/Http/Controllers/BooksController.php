<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Book;

class BooksController extends Controller
{

    public function index(){
        return view('books.all')->with(['books'=>Book::all()]);
    }

    public function create(){
        return view('books.create');
    }

    public function store(Request $request){
        Book::create($request->all());
        return $this->index();
    }

    public function getIndex(){
        return view('books.index');
    }

    public function postBooks(Request $request){
        if(!$request->has('genre')){
            $books = Book::all();

        }else{
            $genre = $request->get('genre');
            $books = Book::whereIn('genre', $genre)->get();
        }

        return $books;
    }
}
